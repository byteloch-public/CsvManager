﻿namespace Benchmarks;

internal class Program
{
    /// <summary>
    /// dotnet run --project Benchmarks -c Release
    /// </summary>
    static void Main() => BenchmarkRunner.Run<SolutionsBenchmark>();
}
