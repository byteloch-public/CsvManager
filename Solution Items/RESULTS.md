# Benchmark Results Summary

Conducting performance testing on two methods, namely SimpleTransformCsv and TransformCsv, to understand how efficiently they process CSV data. The methods were tested with different inputs, and the results are summarized below.

## Detailed Results

```
BenchmarkDotNet v0.13.10, Windows 11 (10.0.22631.2506/23H2/2023Update/SunValley3)
Intel Core i7-1065G7 CPU 1.30GHz, 1 CPU, 8 logical and 4 physical cores
.NET SDK 7.0.403
  [Host]     : .NET 7.0.13 (7.0.1323.51816), X64 RyuJIT AVX2
  DefaultJob : .NET 7.0.13 (7.0.1323.51816), X64 RyuJIT AVX2
```

| Method                     | Input                | Mean       | Error     | StdDev    | Median     | Rank | Gen0   | Allocated |
|--------------------------- |--------------------- |-----------:|----------:|----------:|-----------:|-----:|-------:|----------:|
| SimpleTransformCsvSolution | count(...)00km2 [40] |   284.7 ns |  11.23 ns |  32.39 ns |   284.2 ns |    1 | 0.1452 |     608 B |
| SimpleTransformCsvSolution | id,na(...)28,11 [47] |   501.1 ns |  31.18 ns |  89.96 ns |   494.2 ns |    2 | 0.2065 |     864 B |
| SimpleTransformCsvSolution | heade(...),NULL [48] |   504.2 ns |  16.52 ns |  46.06 ns |   492.6 ns |    2 | 0.2060 |     864 B |
| TransformCsvSolution       | count(...)00km2 [40] | 1,608.5 ns |  31.95 ns |  55.11 ns | 1,609.1 ns |    3 | 0.4387 |    1840 B |
| TransformCsvSolution       | heade(...),NULL [48] | 3,441.7 ns | 185.80 ns | 547.83 ns | 3,267.9 ns |    4 | 0.5646 |    2368 B |
| TransformCsvSolution       | id,na(...)28,11 [47] | 3,525.4 ns | 332.99 ns | 922.70 ns | 3,269.1 ns |    4 | 0.5455 |    2296 B |

## Summary

In summary, the SimpleTransformCsv method consistently outperforms the TransformCsv method across various inputs:

1. Faster Execution Times: This indicates that the SimpleTransformCsv method is more efficient and completes its processing tasks more quickly than the TransformCsv method.

2. Lower Variability (StdDev): A lower standard deviation (StdDev) suggests that the performance measurements of SimpleTransformCsv are more consistent and have less variability than TransformCsv. In other words, SimpleTransformCsv provides more predictable and stable performance across different inputs.

3. Lower Memory Usage (Gen0, Allocated): A lower Gen0 (generation 0 garbage collection) and Allocated memory suggest that SimpleTransformCsv is more memory-efficient than TransformCsv. Lower memory usage is generally beneficial as it allows the system to handle more throughput and reduces the likelihood of memory-related issues.

In summary, the SimpleTransformCsv method appears to have advantages in terms of both speed and resource usage compared to the TransformCsv method. These advantages make it a more efficient and reliable choice for processing CSV data in various scenarios.

## Analyst

Here are the reasons why the method SimpleTransformCsv would have better performance compared to the TransformCsv method.

## SimpleTransformCsv Method

1. Array Access Optimization: The SimpleTransformCsv method uses direct array access for checking if all cells are empty (Array.TrueForAll) and searching for the "NULL" cell (Array.IndexOf). This direct array access might result in more efficient memory access and computation.

2. Early Exit Optimization: The SimpleTransformCsv method has an early exit condition for the case where all cells in a row are empty. This can improve performance by skipping unnecessary processing for rows with all empty cells.

3. Less Overhead: The SimpleTransformCsv method has fewer steps involving LINQ and additional list manipulations. The direct handling of arrays might reduce overhead compared to the TransformCsv method.

Note: Using the built-in libraries provided by the .NET CLR ensures that, as the .NET framework undergoes updates and releases, our projects will automatically benefit from the improvements made to the runtime. This includes potential performance boosts and optimisations introduced in the updated framework. This should serve as an incentive to uphold the latest .NET framework and its associated NuGet libraries.

### TransformCsv Method

1. LINQ and List Manipulations: The TransformCsv method uses LINQ extensively to manipulate lists (Select, AsEnumerable, etc.). This introduces additional overhead in terms of object creation and method calls, which might impact performance.

2. String.Contains vs. Array.IndexOf: The TransformCsv method uses string.Contains for checking the presence of the "NULL" cell. In contrast, SimpleTransformCsv uses Array.IndexOf, which might be more efficient for large datasets.

3. Additional Indexing: The TransformCsv method introduces an additional layer of indexing (IndexedCsvRow) compared to directly accessing arrays in SimpleTransformCsv.

#### Note about the TransformCsv method:

The `TransformCsv` method leverages the `IndexedCsvRow` struct, which represents a CSV row along with its index. Once instantiated, that instance of `IndexedCsvRow` is immutable.

The concept is to provide a structured representation that aims to enhance code reliability and maintainability by reducing the risk of runtime errors resulting from inadvertent mishandling of data.

While introducing a layer of abstraction, this approach fosters clarity and protects against potential pitfalls, serving as a proactive measure to prevent data-related issues within the runtime environment.

It is important to acknowledge that this approach, while prioritising code integrity, introduces a performance trade-off when compared to the more performance-centric methodology showcased in the `SimpleTransformCsv` method.

In certain scenarios, optimal performance may not always be the sole determinant of overall success.
