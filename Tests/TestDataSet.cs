﻿namespace Tests;

public static class TestDataSet
{
    public static IEnumerable<object[]> GenerateBasicDataSet()
    {
        List<object[]> data = new()
        {
            new object[] { "Example.01", "id,name,age,score\n1,Jack,NULL,12\n17,Betty,28,11", "id,name,age,score\n17,Betty,28,11" },
            new object[] { "Example.02", "header,header\nANNUL,ANNULLED\nnull,NILL\nNULL,NULL", "header,header\nANNUL,ANNULLED\nnull,NILL" },
            new object[] { "Example.03", "country,population,area\nUK,67m,242000km2", "country,population,area\nUK,67m,242000km2" },
            new object[] { "Example.04", "empty,cells\n\n\n", "empty,cells" },
            new object[] { "Example.05", "a,b,c,d,e\n1,2,3,4,5", "a,b,c,d,e\n1,2,3,4,5" },
            new object[] { "Example.06", "ID,Name,Score\nNULL,John,15", "ID,Name,Score" },
            new object[] { "Example.07", "Header1,Header2\nNULL,NULL\nNULL,NULL", "Header1,Header2" },
            new object[] { "Example.08", "data,data,data\n1,NULL,3\n4,5,6", "data,data,data\n4,5,6" },
            new object[] { "Example.09", "HeaderA,HeaderB,HeaderC\n", "HeaderA,HeaderB,HeaderC" },
            new object[] { "Example.10", "HeaderX,HeaderY,HeaderZ", "HeaderX,HeaderY,HeaderZ" },
            new object[] { "Example.11", "", "" }
        };
        return data;
    }
}
