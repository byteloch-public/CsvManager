namespace Tests;

[TestClass]
public class UnitTest
{
    public required TestContext TestContext { get; init; }

    [TestMethod]
    [DynamicData(nameof(TestDataSet.GenerateBasicDataSet), typeof(TestDataSet), DynamicDataSourceType.Method)]
    public void TestMethod(string display_name, string input, string expected)
    {
        TestContext.WriteLine($" > {display_name}");
        string results = CsvManager.TransformCsv(input);
        Assert.AreEqual(expected, results);
    }

    [TestMethod]
    [DynamicData(nameof(TestDataSet.GenerateBasicDataSet), typeof(TestDataSet), DynamicDataSourceType.Method)]
    public void TestSimpleMethod(string display_name, string input, string expected)
    {
        TestContext.WriteLine($" > {display_name}");
        string results = CsvManager.SimpleTransformCsv(input);
        Assert.AreEqual(expected, results);
    }
}
